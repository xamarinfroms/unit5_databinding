﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    public partial class Info : ContentPage
    {
        public static InfoData data;
        public Info()
        {
            data = new InfoData();
            this.BindingContext = data;
            InitializeComponent();

            //Binding bind = new Binding()
            //{
            //     Source= data,
            //     Path=nameof(data.Name)
            //};

            //this.txtName.SetBinding(Entry.TextProperty, bind);
         
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            data.Name = this.txtName.Text;
            data.Email = this.txtEmail.Text;
           // ....
        }
    }
}
